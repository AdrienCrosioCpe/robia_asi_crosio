#!/usr/bin/env python
from flask import Flask, render_template, Response

import cv2

INPUT_FILE="voitures.mp4"

app = Flask(__name__)

def gen():
    vs = cv2.VideoCapture(INPUT_FILE)
    """Video streaming generator function."""
    while True:
        (grabbed, frame) = vs.read()
        ret, jpeg = cv2.imencode(".jpg", frame)
        # print("after get_frame")
        if jpeg is not None:
            frame = jpeg.tobytes()
            yield (b'--frame\r\n'
                b'Content-Type:image/jpeg\r\n'
                b'Content-Length: ' + f"{len(frame)}".encode() + b'\r\n'
                b'\r\n' + frame + b'\r\n')
        else:
            print("frame is none")



@app.route("/video_feed")
def video_feed():
    """Video streaming route. Put this in the src attribute of an img tag."""
    return Response(gen(),mimetype="multipart/x-mixed-replace; boundary=frame")


if __name__ == "__main__":
    app.run(host="0.0.0.0", port=5010, threaded=True)