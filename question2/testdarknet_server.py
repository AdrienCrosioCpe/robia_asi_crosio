#!/usr/bin/env python
from flask import Flask, render_template, Response
import numpy as np
import time
import cv2
import testdarknet
from flask_socketio import SocketIO, join_room, leave_room,send, emit

app = Flask(__name__)
socketio = SocketIO(app)


INPUT_FILE='http://10.0.2.15:5010/video_feed'

def gen():
    """Video streaming generator function."""
    
    for image in testdarknet.treatmentImage(INPUT_FILE,True):
        ret, jpeg = cv2.imencode(".jpg", image)
        # print("after get_frame")
        if jpeg is not None:
            frame = jpeg.tobytes()
            yield (b'--frame\r\n'
                b'Content-Type:image/jpeg\r\n'
                b'Content-Length: ' + f"{len(frame)}".encode() + b'\r\n'
                b'\r\n' + frame + b'\r\n')
        else:
            print("frame is none")


@app.route("/video_feed")
def video_feed():
    """Video streaming route. Put this in the src attribute of an img tag."""
    return Response(gen(),mimetype="multipart/x-mixed-replace; boundary=frame")

@app.route("/")
def index():
    """Video streaming home page."""
    return render_template("index.html")

@socketio.on('data')
def on_data(data):
    for detect in testdarknet.treatmentImage(INPUT_FILE, False):
        print(detect)
        emit('data',detect)

if __name__ == '__main__':
    socketio.run(app)


# if __name__ == "__main__":
#     app.run(host="0.0.0.0", port=5011, threaded=True)