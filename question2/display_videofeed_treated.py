import numpy as np
import time
import cv2
import socketio

INPUT_FILE='http://127.0.0.1:5000/video_feed'
vs = cv2.VideoCapture(INPUT_FILE)
cnt =0
while True:
    cnt+=1
    print ("Frame number", cnt)
    try:
        (grabbed, image) = vs.read()
    except:
        break
    # show the output image
    cv2.imshow("output", cv2.resize(image,(800, 600)))